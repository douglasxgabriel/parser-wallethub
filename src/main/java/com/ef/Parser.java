package com.ef;

import com.ef.app.ParserApplication;

/**
 * @author douglasgabriel
 */
public class Parser{
    
    public static void main(String[] args) {
        try {
            new ParserApplication().run(args);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }
    }
    
}

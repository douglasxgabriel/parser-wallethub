package com.ef.app.services;

import com.ef.app.models.BlockedIp;
import com.ef.app.models.Log;
import com.ef.app.parser.LogParser;
import com.ef.app.repositories.BlockedIpRepository;
import com.ef.app.repositories.BlockedIpRepositoryImpl;
import com.ef.app.repositories.LogRepository;
import com.ef.app.repositories.LogRepositoryImpl;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * @author douglasgabriel
 */
public class LogServiceImpl implements LogService {

    private LogParser parser = new LogParser();
    private LogRepository logRepository = new LogRepositoryImpl();
    private BlockedIpRepository blockedIpRepository = new BlockedIpRepositoryImpl();

    private static final String DAILY_DURATION = "daily";
    private static final String HOURLY_DURATION = "hourly";
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd.HH:mm:ss");

    @Override
    public List<Log> loadFromFile(String filePath) {
        List<Log> result = parser.fromFile(filePath).parse();
        logRepository.save(result);
        return result;
    }

    public List<String> searchIps(String fromDate, String requestNumber, String duration) {
        LocalDateTime beginDate = LocalDateTime.parse(fromDate, DATE_FORMATTER);
        LocalDateTime toDate = null;

        if (DAILY_DURATION.equals(duration)) {
            toDate = beginDate.plusDays(1);
        } else if (HOURLY_DURATION.equals(duration)) {
            toDate = beginDate.plusHours(1);
        }
        
        List<String> result = logRepository.searchIps(
                beginDate,
                Integer.parseInt(requestNumber),
                toDate);
        
        generateBlockedIps(fromDate, requestNumber, duration, result);
        
        return result;
    }
    
    private void generateBlockedIps (String fromDate, String requestNumber, 
            String duration, List<String> ips){
        
        ips.forEach(ip -> {
            BlockedIp blockedIp = new BlockedIp(
                    ip, 
                    "Blocked due to exceeded threshold of " + requestNumber
                    + " requests in a interval of one " 
                    + (HOURLY_DURATION.equals(duration) ? "hour" : "day")
                    + " beginning at " + fromDate
            );
            
            blockedIpRepository.save(blockedIp);
        });
    }
}

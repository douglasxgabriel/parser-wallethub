package com.ef.app.services;

import com.ef.app.models.Log;
import java.util.List;

/**
 * @author douglasgabriel
 */
public interface LogService {
    
    List<Log> loadFromFile(String filePath);
    
    List<String> searchIps(String fromDate, String requestNumber, 
            String duration);
}

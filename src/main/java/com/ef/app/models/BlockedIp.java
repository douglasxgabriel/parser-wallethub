package com.ef.app.models;

/**
 * @author douglasgabriel
 */
public class BlockedIp {

    private Long id;
    private String ip;
    private String comment;

    public BlockedIp() {
    }

    public BlockedIp(String ip, String comment) {
        this.ip = ip;
        this.comment = comment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}

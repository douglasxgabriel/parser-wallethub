package com.ef.app.models;

import java.time.LocalDateTime;

/**
 * @author douglasgabriel
 */
public class Log {

    private Long id;
    private LocalDateTime date;
    private String ip;
    private String request;
    private String userAgent;
    private Integer status;

    public Log() {
    }

    public Log(LocalDateTime date, String ip, String request, String userAgent, Integer status) {
        this.date = date;
        this.ip = ip;
        this.request = request;
        this.userAgent = userAgent;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Log{" + "date=" + date + ", ip=" + ip + ", request=" + request + ", userAgent=" + userAgent + ", status=" + status + '}';
    }

}

package com.ef.app.parser;

import com.ef.app.models.Log;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * @author douglasgabriel
 */
public class LogParser {

    private static final String SPLIT_TOKEN = "\\|";
    private static final int COL_DATE = 0;
    private static final int COL_IP = 1;
    private static final int COL_REQUEST = 2;
    private static final int COL_STATUS = 3;
    private static final int COL_USER_AGENT = 4;
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
    
    private Stream<String> stream;

    public LogParser fromFile(String path) {
        this.stream = new BufferedReader(
                new InputStreamReader(
                        getClass().getResourceAsStream(path)
                )
        ).lines();
        return this;
    }

    public List<Log> parse() {
        List<Log> result = new ArrayList<>();
        this.stream.forEach(line -> result.add(parseLine(line)));
        return result;
    }
    
    private Log parseLine(String line){
        String[] infos = line.split(SPLIT_TOKEN);
        
        return new Log(
                LocalDateTime.parse(infos[COL_DATE], DATE_FORMATTER),
                infos[COL_IP],
                infos[COL_REQUEST],
                infos[COL_USER_AGENT],
                Integer.parseInt(infos[COL_STATUS])
        );
    }

}

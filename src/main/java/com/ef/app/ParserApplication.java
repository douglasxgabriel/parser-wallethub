package com.ef.app;

import com.ef.app.exceptions.ExceptionBuilder;
import com.ef.app.services.LogService;
import com.ef.app.services.LogServiceImpl;
import com.ef.cli.CLIApplication;
import java.util.List;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;

/**
 * @author douglasgabriel
 */
public class ParserApplication extends CLIApplication {

    private static final String LOGS_SOURCE_FILE_PATH = "/access.log";
    
    /**
     * @see CLIApplication#defineOptions()
     */
    @Override
    protected List<Option> defineOptions() {
        return ParserApplicationOptions.getAll();
    }

    /**
     * @see CLIApplication#runApplication(CommandLine)
     */
    @Override
    protected void runApplication(CommandLine cmd) throws Exception{
        LogService logService = new LogServiceImpl();
        
        if (cmd.hasOption(ParserApplicationOptions.PARSE.getLongOpt())) {
            System.out.println("Parsing logs from file to MySQL");
            logService.loadFromFile(LOGS_SOURCE_FILE_PATH);
            System.out.println("Parse completed successfully\n");
        }
        
        validateRequiredArgs(cmd);
        
        logService.searchIps(
                cmd.getOptionValue(ParserApplicationOptions.START_DATE.getLongOpt()),
                cmd.getOptionValue(ParserApplicationOptions.THRESHOLD.getLongOpt()),
                cmd.getOptionValue(ParserApplicationOptions.DURATION.getLongOpt())
        ).forEach(System.out::println);
    }
    
    /**
     * Verifies if all the required params were filled.
     * @param cmd object that represents the arguments of the user
     * @throws Exception if some param is missing.
     */
    private void validateRequiredArgs(CommandLine cmd) throws Exception{
        ExceptionBuilder exBuilder = new ExceptionBuilder();
        
        if (!cmd.hasOption(ParserApplicationOptions.START_DATE.getLongOpt())){
            exBuilder.addMissingOption(ParserApplicationOptions.START_DATE);
        }
        if (!cmd.hasOption(ParserApplicationOptions.THRESHOLD.getLongOpt())){
            exBuilder.addMissingOption(ParserApplicationOptions.THRESHOLD);
        }
        if (!cmd.hasOption(ParserApplicationOptions.DURATION.getLongOpt())){
            exBuilder.addMissingOption(ParserApplicationOptions.DURATION);
        }
        
        if (exBuilder.hasMissingOptions())
            throw exBuilder.build();
    }

}

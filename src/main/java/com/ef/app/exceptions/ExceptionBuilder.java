package com.ef.app.exceptions;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.cli.Option;

/**
 * @author douglasgabriel
 */
public class ExceptionBuilder {

    private List<Option> missingOptions = new ArrayList<>();
    
    public ExceptionBuilder addMissingOption(Option opt){
        missingOptions.add(opt);
        return this;
    }
    
    public boolean hasMissingOptions(){
        return missingOptions.size() > 0;
    }
    
    public Exception build(){
        if (hasMissingOptions()){
            return mountMissingAttributesExeption();
        }
        
        return null;
    }

    private Exception mountMissingAttributesExeption() {
        StringBuilder message = new StringBuilder()
                .append("There are some missing options: \n");
        
        missingOptions.forEach(option -> {
            message.append(option.getLongOpt()).append("\n");
        });
        
        return new MissingAttributesException(message.toString());
    }
    
}

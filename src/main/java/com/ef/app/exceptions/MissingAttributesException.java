package com.ef.app.exceptions;

/**
 * @author douglasgabriel
 */
public class MissingAttributesException extends Exception{

    public MissingAttributesException(String message){
        super(message);
    }
    
}

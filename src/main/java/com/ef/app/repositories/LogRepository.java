package com.ef.app.repositories;

import com.ef.app.models.Log;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author douglasgabriel
 */
public interface LogRepository {
    
    void save(List<Log> logs);
    
    List<String> searchIps(LocalDateTime fromDate, int requestNumber, LocalDateTime toDate);
    
}

package com.ef.app.repositories;

import com.ef.app.models.BlockedIp;

/**
 * @author douglasgabriel
 */
public interface BlockedIpRepository {
    
    void save(BlockedIp blockedIp);
    
}

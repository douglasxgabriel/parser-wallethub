package com.ef.app.repositories;

import com.ef.app.models.BlockedIp;
import com.ef.app.repositories.connections.DatabaseConnection;
import com.ef.app.repositories.connections.MySqlConnection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author douglasgabriel
 */
public class BlockedIpRepositoryImpl implements BlockedIpRepository {

    private final DatabaseConnection databaseConnection = new MySqlConnection();

    @Override
    public void save(BlockedIp blockedIp) {
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO blocked_ip (ip, comment) ")
           .append("VALUES (?, ?)");

        try(PreparedStatement statement = databaseConnection.getConnection()
                .prepareStatement(sql.toString())){
            statement.setString(1, blockedIp.getIp());
            statement.setString(2, blockedIp.getComment());
            
            statement.execute();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

}

package com.ef.app.repositories;

import com.ef.app.models.Log;
import com.ef.app.repositories.connections.DatabaseConnection;
import com.ef.app.repositories.connections.MySqlConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author douglasgabriel
 */
public class LogRepositoryImpl implements LogRepository{

    private final DatabaseConnection databaseConnection = new MySqlConnection();
    
    @Override
    public void save(List<Log> logs){
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO log (date, ip, request, user_agent, status) ");
        sql.append("VALUES (?, ?, ?, ?, ?)");
        
        Connection con = databaseConnection.getConnection();
        try(PreparedStatement statement = con.prepareStatement(sql.toString())){
            for (Log log : logs){
                statement.setString(1, log.getDate().toString());
                statement.setString(2, log.getIp());
                statement.setString(3, log.getRequest());
                statement.setString(4, log.getUserAgent());
                statement.setInt(5, log.getStatus());
                statement.addBatch();
            }
            statement.executeBatch();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public List<String> searchIps(LocalDateTime fromDate, int requestNumber, LocalDateTime toDate) {
        StringBuilder sql = new StringBuilder("");
        sql.append("SELECT ip ")
           .append("FROM log ")
           .append("WHERE date between ")
           .append("? and ? ")
           .append("GROUP BY ip ")
           .append("HAVING count(ip) > ? ");
                
        Connection con = databaseConnection.getConnection();
        List<String> ips = new ArrayList<>();
        
        try(PreparedStatement statement = con.prepareStatement(sql.toString())){
            statement.setString(1, fromDate.toString());
            statement.setString(2, toDate.toString());
            statement.setInt(3, requestNumber);
            
            ResultSet result = statement.executeQuery();
            
            while(result.next()){
                ips.add(result.getString("ip"));
            }
            
            return ips;
        }catch(SQLException e){
            e.printStackTrace();
        }
        
        return ips;
    }
    
}

package com.ef.app.repositories.connections;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author douglasgabriel
 */
public class MySqlConnection implements DatabaseConnection{

    private Connection connection;
    private static final int NUM_MAX_CONNECTIONS = 1;
    private int connection_index = 0;
    private static final DatabaseProperties PROPERTIES = new DatabaseProperties();
    
    private List<Connection> connections_pool = new ArrayList<>();
    
    public MySqlConnection(){
        try{
            Class.forName(PROPERTIES.getDrive());
            for (int i = 0; i < NUM_MAX_CONNECTIONS; i++){
                connections_pool.add(DriverManager.getConnection(
                        PROPERTIES.getUrl(), 
                        PROPERTIES.getUser(), 
                        PROPERTIES.getPassword()
                ));
            }
        }catch(ClassNotFoundException | SQLException e){
            e.printStackTrace();
        }
    }
    
    @Override
    public Connection getConnection() {
        Connection result = connections_pool.get(connection_index++);
        
        if (connection_index == NUM_MAX_CONNECTIONS){
            connection_index = 0;
        }
        
        return result;
    }
    
}

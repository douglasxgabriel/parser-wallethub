package com.ef.app.repositories.connections;

import java.io.IOException;
import java.util.Properties;

/**
 * @author douglasgabriel
 */
public class DatabaseProperties {
    
    private static final String DRIVER = "driver";
    private static final String URL = "url";
    private static final String USER = "user";
    private static final String PASSWORD = "password";
    private static final String PROPERTIES_FILE = "/database.properties";
    
    private Properties properties;
    
    public DatabaseProperties(){
        try{
            this.properties = new Properties();
            this.properties.load(getClass().getResourceAsStream(PROPERTIES_FILE));
        }catch (IOException e){
            e.printStackTrace();
        }
    }
        
    public String getDrive(){
        return this.properties.getProperty(DRIVER);
    }
    
    public String getUrl(){
        return this.properties.getProperty(URL);
    }
    
    public String getUser(){
        return this.properties.getProperty(USER);
    }
    
    public String getPassword(){
        return this.properties.getProperty(PASSWORD);
    }
}

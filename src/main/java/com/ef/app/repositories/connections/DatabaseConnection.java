package com.ef.app.repositories.connections;

import java.sql.Connection;

/**
 * @author douglasgabriel
 */
public interface DatabaseConnection {

    Connection getConnection();
    
}

package com.ef.app;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.cli.Option;

/**
 * @author douglasgabriel
 */
public class ParserApplicationOptions {

    public static final Option START_DATE = Option.builder()
            .longOpt("startDate")
            .desc("in yyyy-MM-dd.HH:mm:ss format")
            .hasArg()
            .build();
    
    public static final Option DURATION = Option.builder()
            .longOpt("duration")
            .desc("[hourly, daily]")
            .hasArg()
            .build();
    
    public static final Option THRESHOLD = Option.builder()
            .longOpt("threshold")
            .desc("request quantity")
            .hasArg()
            .build();
    
    public static final Option PARSE = Option.builder()
            .longOpt("parse")
            .desc("runs the log file parse routine")
            .build();
    
    public static final List<Option> getAll(){
        List<Option> result = new ArrayList<>();
        result.add(START_DATE);
        result.add(DURATION);
        result.add(THRESHOLD);
        result.add(PARSE);
        
        return result;
    }
    
}

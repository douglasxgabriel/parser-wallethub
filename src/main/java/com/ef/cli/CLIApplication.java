package com.ef.cli;

import java.util.List;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * @author douglasgabriel
 */
public abstract class CLIApplication {
    
    /**
     * Starts the CLI application setting the avaliable options
     * displaying help if it needs and running the application's
     * process.
     * 
     * @param args the arguments setted on the application start
     * @throws ParseException if occur some problem with the parse of the
     *  right values into the avaliable options
     * @throws Exception if any other unknow error occur
     */
    public void run(String[] args) throws ParseException, Exception{
        Options opts = buildOptions();
        CommandLine cmd = new DefaultParser().parse(opts, args);
        
        if (mustDisplayHelp(cmd)){
            displayHelp(opts);
            return;
        }
        
        runApplication(cmd);
    }
    
    /**
     * Make the options avaliable and also set the help option
     * 
     * @return the object that represents all the avaliable options
     */
    private Options buildOptions(){
        Options opts = new Options();
        
        opts.addOption(
                Option.builder("h")
                .longOpt("help")
                .required(false)
                .build()
        );
        
        defineOptions().forEach(option -> {
            opts.addOption(option);
        });
        return opts;
    }
    
    /**
     * Verifies if the application help must be displayed.
     * 
     * @param cmd the object that represents the commands from user
     * @return true if the help must be displayed
     */
    private boolean mustDisplayHelp(CommandLine cmd){
        return cmd.hasOption("help") || cmd.getOptions().length == 0;
    }
    
    /**
     * Displays the help containing all the avaliable options and its 
     * descriptions.
     * 
     * @param opts the object that represents all the avaliable options
     */
    private void displayHelp(Options opts){
        new HelpFormatter().printHelp("Parser", opts);
    }
    
    /**
     * Defines the CLI options
     * @return a {@link List} containing the CLI options
     */
    protected abstract List<Option> defineOptions();
    
    /**
     * Must implement the application business
     * 
     * @param cmd object that represents the arguments of the user
     */
    protected abstract void runApplication(CommandLine cmd) throws Exception;
    
}

Parser
======

A Java tool that parses a log file into a MySQL database and provides a set of options to query against this data.

### Requirements

- Java ^1.8
- Maven

### Running

- Build the project running `mvn install`;
- Go to the `/target` folder and run `java -cp "parser.jar" com.rf.Parser`.

### Configuration

This project uses a `database.properties` file that lives in the `src/main/resources` folder to get database connection configurations.

By default it assumes that you have a MySQL database running in `localhost` with a database called `wallethub` with user `root` and the password `123456`.

### Database

```sql
create schema `wallethub`

use `wallethub`;

create table log(
  id integer not null auto_increment,
  date timestamp,
  ip varchar(15),
  request varchar(255),
  user_agent varchar(255),
  status INT,
  primary key (id)
);

create table blocked_ip (
  id integer not null auto_increment,
  ip varchar(15),
  comment text,
  primary key (id)
);
```
#### SQL queries examples

- query to find IPs that mode more than a certain number of requests for a given time period.
```sql
SELECT ip FROM wallethub.log
WHERE date BETWEEN '2017-01-01.13:00:00' and '2017-01-01.14:00:00'
GROUP BY ip HAVING count(ip) > 100;
```

- query to find requests made by a given IP
```sql
SELECT request FROM wallethub.log
WHERE ip LIKE "192.168.106.134";
```

### Options

- **--parse (optional)**: runs the log file parse routine, that loads the logs from file and save them in MySql database (it could takes several minutes);

- **--duration**: amount of time from what was set in `--startDate` (possible values: hourly, daily)

- **--startDate**: initial timestamp to search for (in `yyyy-MM-dd.HH:mm:ss` format)

- **--threshold**: request quantity to search for